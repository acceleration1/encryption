package main

import (
	"crypto/sha1"
	"fmt"
	"time"
)

var (
	salt = fmt.Sprintf("%d", time.Now().UnixNano())
)

func hashing(text []byte) ([]byte, error) {
	saltedText := fmt.Sprintf("text %s , salt %s ", text, salt)
	_, err := sha1.New().Write([]byte(saltedText))
	hashedText := sha1.New().Sum(nil)

	return hashedText, err
}
