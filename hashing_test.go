package main

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestHashing2(t *testing.T) {
	data := []byte("Hi, this message should be encrypted, because it contains all about us, no one should know it :)")

	hashed, err := hashing(data)
	assert.NoError(t, err)
	assert.NotEmpty(t, hashed)
	fmt.Println(string(hashed))
}
