package main

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestRSA(t *testing.T) {
	privateKey, publicKey, err := GenerateKeyPair()

	assert.NoError(t, err)
	assert.NotNil(t, privateKey)
	assert.NotNil(t, publicKey)

	data := []byte("Hi, this message should be encrypted, because it contains all about us, no one should know it :)")

	encrypted, err := RSAEncrypt(publicKey, data)
	assert.NoError(t, err)
	assert.NotEmpty(t, encrypted)

	decrypted, err := RSADecrypt(privateKey, encrypted)
	assert.NoError(t, err)
	assert.NotEmpty(t, decrypted)

	fmt.Println(string(decrypted))
}
