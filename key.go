package main

import (
	"crypto/rand"
	"crypto/rsa"
)

const (
	// rsaKeySize is a constant size
	rsaKeySize = 2048
)

// GenerateKeyPair is a method
func GenerateKeyPair() (*rsa.PrivateKey, *rsa.PublicKey, error) {
	privateKey, err := rsa.GenerateKey(rand.Reader, rsaKeySize)
	if err != nil {
		return nil, nil, err
	}

	return privateKey, &privateKey.PublicKey, nil
}
